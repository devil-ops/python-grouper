#!/usr/bin/env python3

import sys
import json
import logging

from pygrouper.helpers import get_credentials
from pygrouper.grouper import api


def main():
    if len(sys.argv) != 2:
        sys.stderr.write("Usage: %s group\n" % sys.argv[0])
        sys.exit(2)

    logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)

    credentials = get_credentials()
    grouper = api(
        server="groups.oit.duke.edu",
        username=credentials["username"],
        password=credentials["password"],
    )
    print(grouper.lookup_group(sys.argv[1]).get_member_netids())
    return 0


if __name__ == "__main__":
    sys.exit(main())
