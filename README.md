# python-grouper

Python wrapper to grouper utilities

Heavily influenced (slash lovingly stolen) from
[python-grouper-tools](https://gitlab.oit.duke.edu/devil-ops/python-grouper-tools).
Eventually hoping to make this the backend library for the python-grouper-tools
frontend

## Setup

Make sure you have a ~/.grouper.yaml file like:

```yaml
---
'groups.oit.duke.edu':
  username: foo
  password: bar
```
