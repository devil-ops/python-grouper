import yaml
import os


def get_credentials(*args, **kwargs):
    server = kwargs.get('server', 'groups.oit.duke.edu')

    config_file = os.path.expanduser("~/.grouper.yaml")
    with open(config_file, 'r') as stream:
        return yaml.load(stream)[server]
