import json
import logging


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i : i + n]


class Group(object):
    def __init__(self, api, *args, **kwargs):
        self.raw_data = kwargs
        self.api = api

    def get_member_netids(self):
        netids = []
        all_subjects = self.raw_data["wsSubjects"]
        total_subjects = len(all_subjects)
        count = 0
        for item in all_subjects:
            count += 1

            # Skip groups
            if item["sourceId"] == "g:gsa":
                continue

            logging.debug("(%s/%s) Looking up %s" % (count, total_subjects, item["id"]))
            netid = self.api.find_netid_from_dukeid(item["id"])
            if netid:
                netids.append(netid)
            else:
                print(item)

        netids.sort()
        return netids

    def get_batch_member_netids(self):
        all_subjects = self.raw_data["wsSubjects"]
        netids = sorted(self.api.find_batch_netids_from_dukeids(all_subjects))
        return netids

    def get_subgroups(self):
        subgroups = []
        all_subjects = self.raw_data["wsSubjects"]

        for item in all_subjects:

            # Skip everything other than groups
            if item["sourceId"] != "g:gsa":
                continue

            subgroup = item['name']
            logging.debug(f"Adding group {subgroup}")

            subgroups.append(subgroup)

        return subgroups

    def __getattr__(self, key):
        return self.raw_data[key]

    def __repr__(self):
        title = self.raw_data["wsGroup"]["name"]
        return u"<Grouper.Group.%s object>" % title

    def json(self):
        return json.dumps(self.raw_data)
