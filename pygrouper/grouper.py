import json
import requests
from pygrouper.models import Group
import logging
from ldap3 import Connection
import sys


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i : i + n]


class api(object):
    def __init__(self, *args, **kwargs):
        self.auth = (kwargs["username"], kwargs["password"])
        version = "v2_1_500"
        self.api_base = "https://%s/grouper-ws/servicesRest/json/%s/" % (
            kwargs["server"],
            version,
        )

    def lookup_group(self, *args, **kwargs):
        group_name = args[0]
        result = self.query("groups/%s/members" % group_name, **kwargs)["WsGetMembersLiteResult"]
        return Group(api=self, **result)

    def lookup_group_data(self, group_name, **kwargs):
        member_filter = kwargs.get('member_filter', 'all')
        payload = {'WsRestGetMembersRequest': {'wsGroupLookups': [{'groupName': group_name}], 'subjectAttributeNames': ['name'], 'memberFilter': member_filter}}
        try:
            result = self.query("groups", data=json.dumps(payload), **kwargs)["WsGetMembersResults"]['results'][0]
        except Exception as e:
            logging.exception(e)
            raise
        return Group(api=self, **result)

    def lookup_group_structure(self, group_name: str):
        group_structure = {}
        direct_descendents = self.lookup_group_data(group_name, member_filter='immediate')
        direct_subgroups = direct_descendents.get_subgroups()
        direct_members = direct_descendents.get_member_netids()
        group_structure[group_name] = {"direct_members": direct_members, "direct_subgroups": []}
        for subgroup in direct_subgroups:
            group_structure[group_name]["direct_subgroups"].append(self.lookup_group_structure(subgroup))
        return group_structure

    def find_netid_from_dukeid(self, search):
        """
        query LDAP to get the NetID of an ID number as used by Grouper
        """
        ldaps = Connection("ldaps://ldap.duke.edu", auto_bind=True)
        ldaps.search("dc=duke,dc=edu", "(duDukeID=" + search + ")", attributes=["uid"])
        if len(ldaps.entries) < 1:
            sys.stderr.write("No netid found for {}\n".format(search))
            return
        else:
            self.netid = ldaps.entries[0].uid.values[0]
            return self.netid

    def find_batch_netids_from_dukeids(self, searches):
        """
        query LDAP to get the NetID of an ID number as used by Grouper
        """
        netids = []
        total = len(searches)
        incr = 40
        ldaps = Connection("ldaps://ldap.duke.edu", auto_bind=True)
        count = 0
        for chunk in chunks(searches, incr):
            count += incr
            logging.debug("Processing %s/%s" % (count, total))
            batch_uids = []
            chunk_uids = []
            for item in chunk:
                # Skip groups here
                if item["sourceId"] == "g:gsa":
                    continue
                batch_uids.append("(duDukeID=%s)" % item["id"])
                chunk_uids.append(item["id"])
            ldap_q = "(|%s)" % ("".join(batch_uids))
            ldaps.search("dc=duke,dc=edu", ldap_q, attributes=["uid", "duDukeID"])
            for thing in ldaps.entries:
                duid = thing.dudukeid.values[0]
                netid = thing.uid.values[0]
                netids.append(netid)
                chunk_uids.remove(duid)
            if len(chunk_uids) != 0:
                raise Exception("UnknownNetIDforUID %s" % chunk_uids)
        return netids

    def query(self, *args, **kwargs):
        """
        :param path: Path to query
        :type path: str
        :param verb: VERB to use for query, default GET
        :verb type: str
        :param params: parameters to use for query
        :params type: dict
        :param data: Data to send up in JSON format
        :data type: dict
        :return: Web call results from json
        :rtype: dict
        """

        path = args[0]
        verb = kwargs.get("verb", "GET")
        params = kwargs.get("params", {})
        data = kwargs.get("data", {})

        headers = {"Content-Type": "text/x-json;charset=UTF-8"}

        full_url = "%s%s" % (self.api_base, path)
        if verb == "GET":
            results = requests.get(
                full_url, params=params, auth=self.auth, headers=headers, data=data
            )
        else:
            results = requests.request(
                verb,
                full_url,
                params=params,
                json=data,
                auth=self.auth,
                headers=headers,
            )

        return results.json()
